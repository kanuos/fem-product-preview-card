# Frontend Mentor - Product preview card component solution

This is a solution to the [Product preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/product-preview-card-component-GO7UmttRfa)

This project uses **HTML** and **vanilla CSS** to implement a *mobile first*, *responsive* design. The screenshot of the finished product can be seen below.

![Design screenshot for the QR code component coding challenge](./Screenshot.png)


### Tech stack:

- [x] HTML to create the structure
- [x] vanilla CSS to design the webpage
- [x] Grid layouts to implement the *block* sections
- [x] Flexbox to structure the `card content` texts
- [x] BEM naming convention in writing CSS


### Process

- Firstly, to start off the project, I navigated to a newly created directory and created an empty git repo inside using `git init` command. The **FrontEnd Mentor Product preview card challenge** repository is cloned in the created repo.
- Next, all the unused files, inline CSS etc were removed. **Montserrat**, **Fraunces** font families were added to the project using *Google Fonts CDN*. All the colors provided in the *style-guide.md* was added to the `:root` of the CSS. 
- A CSS reset was implemented. Font size of the page was set to 14px so that `1rem == 14px`
- The main page sanning the full viewport with appropriate background color was implemented. 
- The `card` component was implemented using *mobile first* design. Grid layout was implemented for the contents of the card. The contents were added keeping *responsive design* in mind.
- Added media query to look according to the `designs` using `375px` for `mobile` and `1440px` for `desktop` breakpoints. The static page was tested on various viewports/devices using the Firefox mobile layout viewer. 
- A screenshot of the final result was taken using **Firefox screenshot** and was added to the project.


### Tools used

- VS Code as the Code Editor
- Live server VS Code extension to create a dev server
- Prettier pluggin for code formatting


### Links

- Solution URL: [Gitlab](https://gitlab.com/kanuos/fem-product-preview-card)
- Live Site URL: [Netlify](https://calm-otter-bd96c4.netlify.app/)



### Author

- Frontend Mentor - [@kanuos](https://www.frontendmentor.io/profile/kanuos)
- GitHub - [@ykanuos](https://github.com/kanuos)
- GitHub - [@ykanuos](https://gitlab.com/kanuos)

